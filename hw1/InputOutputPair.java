/**
 * This example is nifty because it demonstrates a case in which 0CFA yields better results than
 * other call graph algorithms. Other analysis algorithms will assume that any implementation of 
 * "doSomething" can be called from any location in the program, but this is not the case. 
 * D_doSomething cannot be reached in this program, and certain algorithms might not pick up on this fact.
 */

class InputOutputPair {

    public static void main(String[] args) {       
        
        A x;
        int index;
               
        x = new A();
        index = 0;
        
        while (index < 3) {
            x = x.doSomething();
            index = index + 1;
        }        
               
    }
    
}

class A {
    public A doSomething(){
        System.out.println(1);
        return new B();
    }
}

class B extends A {
    public A doSomething(){
        System.out.println(2);
        return new C();
    }
}

class C extends A {
    public A doSomething(){
        System.out.println(3);
        return new A();
    }
}

class D extends A {
    public A doSomething(){
        System.out.println(4);
        return new D();
    }
}



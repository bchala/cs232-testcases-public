/*
Interesting things about this testcase:
- Deals with an overriden method interacting with its parent
- Creates a cycle in the call graph (A_m -> B_m, B_m -> A_m)
- An over-conservative approach would result in a graph with self-loops as well,
but the solution given by 0CFA should not have these.
*/

class Main {
    public static void main(String[] a){
        A alpha;
        B beta;
        alpha = new A();
        beta = new B();
        System.out.println(alpha.m(3, beta));
    }
}

class A {
    public int m(int x, A a) {
        int num;
        if (x < 0) {
            num = 1;
        } else {
            num = a.m(x-1, this);
        }

        return num;
    }
}

class B extends A {
    public int m(int x, A a) {
        int num;
        if (x < 0) {
            num = 2;
        } else {
            num = a.m(x-1, this);
        }

        return num;
    }
}
